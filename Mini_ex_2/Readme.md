**Mini-Ex 2**

**Describe your program and what you have used and learnt**

I programmed two emojis; one man breastfeeding a baby and  one pregnant man. I choose to do it simple, as i wanted to understand every step on the way, as I confused myself the last time. Thus I choose simple shapes as circles, squares and ellipses. I also used points and lines to make hair and beard, to which I had to use the function stroke and strokeWeight - this I learned along the way. I also learned how to make lines/strokes to appear and disappear, as I was frustrated that you could see the lines between my different shapes, but at the same time the lines for the hair had to appear. 
Speaking of the hair, I though it very difficult that I had to use x1,y1,x2,y2 to make a line, and therefore the first emoji only has 5 hairs/lines. 

In terms of the color I choose to make the background color static pink, and otherwise different fill-in colors on the emojis. 

On the second emoji I choose to put a hat on him, as I had trouble with the hair on the other emoji. I copied the hat emoji on the website Emojipedia, and then resized it so it could fit onto my emoji, 
 
**What have you learnt from reading the assigned reading? How would you put your emoji into a wider cultural context that concerns identification, identity, race, social, economics, culture, device politics and beyond?**

The text assigned for this week was interesting to read, is it presented a new perspective on the use of emojis. It presented emojis as a language, which you can use to decode representations of people. The language of emojis has expanded in the last years, as there must be a representation of everyone – first it started with the black people, whereas many others wanted to be among the emoji library. This trend lead to many discussions concerning race, sex, sexuality, etc. Now we can choose between skin-colors, hair-color, age, etc. What is next? 

My two emojis could be next – I focused on the emojis we already had and with this information, I put it in perspective to our culture. The first thing that jumped to my mind, was the woman pregnant and the woman breastfeeding. We have seen a pregnant man – and yes, he used to be a woman, but he was now a man. So I thought: I should make a pregnant man. And naturally, and just to provoke a bit more, I chose to create an emoji consisting of a man with breast, breastfeeding a baby. 


Freya Wattez, Æstetisk Software Programmering, 2. semester

Screenshot of atom-live-server:

[ScreenShot](Skærmbillede.png)

Screenshot of my coding-proces: 

[ScreenShot](Skærmbillede 1.png)

[ScreenShot](Skærmbillede 2.png)

[ScreenShot](Skærmbillede 3.png)

[ScreenShot](Skærmbillede 4.png)

[ScreenShot](Skærmbillede 5.png)



Link to my atom-live-server 

[Link](https://gl.githack.com/Freyaw/miniex1/raw/master/MiniX_2/empty-example/index.html)


