function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(10);
  rectMode(CENTER);

//baggrund
let c = color('magenta');
  background(c);
}

function draw() {
  noStroke();

  //Første emoji

//mandens hovede
fill(255, 204, 0);
circle(400,300,100);


//mandens hals
square(400,425,60);


stroke(0); //Stroke sat til sort
//mandens hår
strokeWeight(1);
line(400, 215, 400, 170);
line(380, 215, 360, 170);
line(420, 215, 440, 170);
line(440, 220, 470, 170);
line(360, 210, 320, 170);

//skæg
strokeWeight(4);
point(350, 350);
point(380, 350);
point(370,340);
point(360,356);
point(400,360);
point(410,370);
point(330,340);
point(370,380);
point(385,380);
point(379,370);
point(450,370);
point(460,374);
point(430,360);
point(460,340);
point(450,345);
point(440,380);
point(430,375);
point(400,345);


noStroke();
//øjne
fill(255);
circle(430,290,12);
fill(0);
circle(430,290,6);

fill(255);
circle(370,290,12);
fill(0);
circle(370,290,6);

//mandens krop
fill('rgb(0,0,255)');
ellipse(400, 525, 210, 235);

fill(255, 204, 0);
//mandens venstre arm
ellipse(490,550,50,160);

//mandens højre arm
ellipse(310,550,50,180);

fill(255, 204, 0);
//bryst
circle(440,510, 30);

//bryst
circle(370,510, 30);

//nipples
fill(255,120,1);
circle(370,515,7);

//nipples
fill(255,120,1);
circle(440,515,7);



//Babys krop
fill('hsl(160, 100%, 50%)');
ellipse(380, 570, 200, 100);

//babys hoved
fill(255, 304, 0);
circle(470, 560, 50,50);

fill(255, 204, 0);
//mandens ene underarm
ellipse(385,623,180,50);

//mandens ene underarm
ellipse(410,623,180,50);



//anden emoji
//mandens hovede
fill(255, 204, 0);
circle(1000,300,100);

//kasket
textSize(500);
text('🧢',820,360);

//øje
fill(255);
circle(1075,292,12);
fill(0);
circle(1078,300,6);

//skæg
stroke(0);
strokeWeight(4);
point(1000, 350);
point(1040, 350);
point(1034,340);
point(1060,356);
point(1056,334);
point(1070,367);
point(1010,380);
point(1020,320);
point(1049,380);
point(1037,380);
point(450,370);
point(460,374);
point(430,360);
point(460,340);
point(450,345);
point(440,380);
point(430,375);
point(400,345);

noStroke();
fill(255, 200, 0);
//mandens hals
square(1000,425,60);

fill('rgb(0,0,255)');
//mandens krop
ellipse(1000, 525, 210, 235);

//mandens babybule
ellipse(1070, 550, 170, 185);

fill(255, 200, 0);
//mandens arm
ellipse(1000,510,50,130);
//mandens underarm
ellipse(1060,560,150,50);

}
