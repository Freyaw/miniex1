function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(10);
  rectMode(CENTER);
}

function draw() {
      translate(50, 50);
fill(45,67,87);
  var step = frameCount % 20;
  var angle = map(step, 0, 20, -PI / 4, PI / 4);
background('rgba(100%,0%,100%,0.5)');

  // equivalent to shearX(angle);
  var shear_factor = 1 / tan(PI / 2 - angle);
  applyMatrix(1, 0, shear_factor, 1, 0, 0);
  fill(random(450),'rgba(0,255,0, 0.25)');

  rect(700, 350, 300, 300);

}
