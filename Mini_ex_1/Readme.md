**Mini-Ex 1**

**Describe your first independent coding process**

My first coding process can be described simply as frustrating. When I approached this task, the first thought that emerged was; Coding is difficult and YOU cannot learn it all by yourself. But as I researched on YouTube and on P5JS I got this YOU CAN DO IT-vibe. So I began. And after about 2 minutes I got my first breakdown. I was about to give up, when my co-student helped me by saying: "don't forget to save after each chance". Thus now it was working and I began experimenting. My experiments were inspired by YouTube videos and of course P5JS. I tried to code different shapes, different background colours, different movements, etc. As the process emerged, several failures appeared. But with help from my co-students and my own experimenting, I made a final Atom-server which consists of a big square with a pink background. The square is filled with randomized basic colours, and it moves from left to right continuously.


**How your coding process is differ or similar to reading and writing text?**

This question is a bit difficult to answer, but I would say that there is a small similarity between my coding process and how to read a book. I get flashbacks to when I sat in French class – didn’t understand a thing either – when you had to find the correct grammar, or construct the sentences with words you didn’t even understand. But sometimes you got an enlightening, where suddenly it all fits together – just like when I code, and suddenly I works. You understand it while you don’t understand it – but it works !
I would also compare the coding process with mathematics. To code is to me the same thing as to figure out maths. You have a function which you have to correctly fill out, and if you don’t do it right, there will either come a wrong answer, or in coding, the outcome will be blank – thus also wrong. An example could be; we know that we want to have a background, thus we write: “background(***)”, where *** has to be filled out with the wished colour – just like a mathematical function.


**What is code and coding/programming practice means to you?**

Coding/ programming practice doesn’t mean a lot to me, as I don’t have a lot of knowledge in this field. But in general I see coding as something difficult and incomprehensible that most people don’t know a lot about. I see it as a field which determines how digital artefacts act – how the software is put together. I also think that it’s interesting the importance of Literacy, and the assertion that more people should learn to code, as we live in a digital world, as Annette Vee discuss in “Coding Literacy, Coding for everyone and the legacy of mass literacy”(2017).


Freya Wattez, Æstetisk Software Programmering, 2. semester

[ScreenShot](Skærmbillede.png)


https://gl.githack.com/Freyaw/miniex1/raw/master/MiniX_1/empty-example/index.html



