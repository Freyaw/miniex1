//URl
let api = "https://api.giphy.com/v1/gifs/search?";
let apiKey = "&api_key=dc6zaTOxFJmzC";
let query = "&q=angry+love+diva+hungry+cry+happy";
let input;
let gif;
let song;
let headline;
let maxGifs;
let gifIndex;
let giphy;
let mood;

//Song
// function preload(){
//   song = loadSound('push.mp3');
// }


function setup() {
  song.play();
//URL API
  noCanvas();
  let url = api + apiKey + query;
  giphy = loadJSON(url, gotData);

//button
  button = createButton('Push the button');
      button.style('background-color', '#ff');
      button.position(630, 650);
      button.style('font-size', '20px');
      button.mousePressed(newgif);

//"GIFeelings"
  headline=createDiv("GIFeelings");
      headline.position(580, 10);
      headline.style('font-size', '50px');
      headline.style('color', '#fae');

//"Select your mood today!"
  mood = createDiv("Select your mood today!")
    mood.position(580, 610);
    mood.style('font-size', '25px');
    mood.style('color', '#fae');

//shows the button when the JSON file is loaded
button.hide();
}
//length of the data and startingpoint
function gotData(d) {
  maxGifs = d.data.length;
  gifIndex = 0;
  button.show();
}

//called when the button is pressed
function newgif(){
   gif=createImg(giphy.data[gifIndex].images.original.url);
   gif.position(450,100);
   gif.size(500,500);
   gifIndex++;
   if(gifIndex >= maxGifs) {
     gifIndex = 0;
   }
 }
