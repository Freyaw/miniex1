If the link doesn't work, try going to Annebeltz' gitlab. 
[Runme](https://gl.githack.com/Annebeltz/ap2019/raw/master/Mini_ex9/index.html)


> "The objective in this miniex is to design and implement a program that utilizes web API(s), to learn to code and conceptualize a program collaboratively and to reflect upon the processes of data parsing via API, paying attention to registration, availability/selection/manipulation of data" 

**A title of your program**
GIFeelings

**Who are you collaborating with / which group?**
Group 6:
Annebel Tang Züger @Annebeltz
Amalie Nyegaard Mortensen @amalienyegaard
Rasmus Lundorff-Hansen @RasmusRasmus
Freya Wattez @Freyaw

**What is the program about? which API have you used and why?**

Our group decided to take on the animated images called GIFs (Graphics Interchange Format), more specifically the online database platform Giphy and their immense ever-growing collection of GIFs. Giphy is a quite prominent figure, as a feature on the internet with their expanding reach in collaborations with popular applications like Facebook Messenger, Tinder and Discord. Gifs are becoming more and more involved in user engagement on the internet, partly because of their accessibility, but also because we humans are visual in nature. “the human brain processes images 60,000 times faster than text, and 90% of information transmitted to the brain is visual.” (http://www.t-sciences.com/news/humans-process-visual-data-better visited 5 April 2019
). Additional with their shorter length it makes them more digestible than video. In the basic sense, our program tries to conceptualize Gifs as a communication tool, the different interpretations of these images and the embodiment of previous expression methods compared to GIFs (such as words like LOL,ROFL, LMAO etc.).

We took inspiration from Daniel Shiffman’s videos around his examples on the Giphy platform and how to fetch data from their server using API and tried to make our own version by still including some aspects from his code so we could fetch the data correctly. 

because we thought it was an interesting topic and we had some ideas we wanted to experiment with involving this concept.

In our program, we have a GIF showing in the back and a button labeled “Select your mood today!” on the screen that you can push to spawn a new random GIF, in the category of “angry”, “love”, “diva”, “hungry”, “cry” and “happy” from the Giphy database. In the top of the program, we have a headline with the name of our program “GIFeelings” in a pink colour scheme. We have also added the song "Push the Button" by Sugarbabes, which is playing in the background when viewing the program.  

In this mini ex we used most of our time trying to solve issues occurring when trying to fetch the data for our program and it has been a very troublesome experience. This means that our program isn't as technical comprehensive as our initial intentions, so the conceptualization of our idea is the main focus of this miniex. 

**Can you describe and reflect your process of making this mini exercise in terms of acquiring, processing, using and representing data? How much do you understand this provided data? How do platform providers sort the data and give you the selected data?**

When making the program we went through a couple of difficulties. Understanding and getting the data from the Giphy API file was quite easy because we were the group that did the class presentation on how JSON works. Our understanding of the provided data is good. In order to receive selected data from any API, You have to create an account, to let them know how You use the data and to what purpose, which is required in terms of new privacy and data policies. When creating an account you get an API key that you have to use in order to get the data. After You have got all the information to the URL you link these together and separate them with for example “&” or “&api_key=”. When executing the link you get the JSON file and call it in the sketch file. We touched upon the API as well which helped us reach a broader understanding of both the API and the JSON file in the API. Thereby, the process of pulling the data into our program was easy, but using the data and calling random gifs when pressing the button was rather difficult. Our imagination of how the program was supposed to work is described in the technical description of the program. 

APIs are important in the way that they can give a sense of how data is circulated, made accessible and inaccessible. Data is the most important aspect of API for us, and therefore the question “how the platforms provide us data” is very essential. 
Looking at the text “API practices and paradigms: Exploring the protocological parameters of APIs as key facilitators of sociotechnical forms of exchange” written by Winnie Soon and Erik Snodgrass, they highlight the following: 

*“… An API provides methods of controlled access to computational components, creating a standardized grammar of functionality in order to facilitate forms of exchange between various components and agents in a manner that typically helps to make them interoperable and independent of their respective implementations."* (Snodgrass & Soon, 2019)

API provides methods with the functionality of the facilitation between various components and agents. This is controlled through an API’s establishing of specified procedures, formats and protocols. So if you want to get data from Google, you request some data with using your individually key, whereas Google gets the requested data from the image data bank, and send it back as a response I JSON. 


**What's the significance of APIs in digital culture?**

APIs are a big part of digital culture, which expanded at the beginning of the year 2000 – which was the use of e-commerce such as eBay, Amazon, Salesforce, etc. Additionally, they have been obtained and adapted by new social networks, such as Facebook, Twitter, Instagram, etc., and these platforms own access to and further circulate their specially oriented forms of exchange. The use of APIs to develop and design technological forms of structured exchanges between software components are very prevailing at the moment. We allow data to be shared, automated, circulated and distributed in a broader 'computational culture', by using social media, Google, etc. 



**Try to formulate a question in relation to web APIs or querying processes that you want to investigate further when you have more time.**

If we had more time, we would like to investigate how different APIs could work together? We only worked with one API and it could have been exciting to combine more sources. In this way, we could create a program with a wider reach and with more features. We would also have liked to investigate the queerness of APIs. How can an API be queer? And to what extent?


**References**
Eisenberg, H. (2014) Humans Process Visual Data Better. Retrieved 5 April 2019 from http://www.t-sciences.com/news/humans-process-visual-data-better. 

Snodgrass, Eric. & Soon, Winnie. (2019) API practices and paradigms: Exploring the protocological parameters of APIs as key facilitators of sociotechnical forms of exchange. Retrieved 5 April 2019 from https://firstmonday.org/ojs/index.php/fm/article/view/9553/7721


![Screeenshot](Skærmbillede_mini_ex9.png)




