On this mini ex i have worked together with Rasmus Lundorff-Hansen. Thus we have created a generative program together. 


**What are the rules in your generative program and describe how your program performs over time?**

We started by looking at Winnies code, where we began experimenting with changing some objects and factors. We choose to implement three roles: 
	1. If Mousepressed, clear setup and draw - start over. 
	2. If grid.pos = 0, turn right 90 degrees. 
	3. If Ant goes left, go one up, else turn left 90 degrees

After we implemented those three roles, we wanted to make our program more specific, whereas you could see what happened - thus that you could follow each step of the program. We didn't totally succed, but when the image hit a new circle, it is shown. The image i just referred to, is the image of Annebel we have chosen to be our "ant" - just to make it a bit funny. Thus the longer you look at our program, the more "Anne-bel Ant" floods the scream. 



**What's generativity and automatism? How does this mini-exercise help you to understand what might be generativity and automatism?**
My notes say that; Generative systems encode subjective aesthetic principles into an external system, such as a piece of software or hardware, or even a completely analog system. This approach provides a computational model of creativity, making it popular with the new generation of digital artists and designers who are turning to code to create new forms of expression. Philip Galanter calles this generative art, as it refers to any art practice where the artist uses a system - just like natural language, a computer program, etc. - which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art. 
Generativity is something that employs randomness, and thereby meant that an generative artist uses a level of randomness, and it's always developing and creating something new. Note that all programming aren't generative art, as our programs aren't developing endless. 
 
Automatism, according to me, is automatic behaviour - and this is the case for objects, computers, etc. 

Thus if we should link those two concepts to our program, it is a generative program which runs automatic. We give our control to the system, where the program function autonomously. There are some rules implented, which makes it controlled random, as it isn't complety random, but still has some random factors in it - it is although still a generated program with automatism. I learned a lot by looking at Winnies code, as that made me understand the core of automatism and generative programming. This is also why we choose to use Winnies program and make our own of it. 

I think that it is very interesting to understand how randomness, generativity and rules combine eachother - the aspect of their relationsship. 



Written and made by:
Freya Wattez, Æstetisk Software Programmering, 2. semester

Screenshot of atom-live-server:
[ScreenShot](Skærmbillede.png)


Link to my atom-live-server:  [Link](https://cdn.staticaly.com/gl/RasmusRasmus/ap-2019-rasmus/raw/master/MiniEx07/Project-miniex07/empty-example/index.html)
