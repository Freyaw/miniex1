
**Who are you collaborating with and how's the collaborative process?**

This week I collaborated with Annebel, thus we have made the same electronic literature. We started brainstorming ideas, whereas we located three things we though were interesting. We wanted to make a program with a thought-through meaning, thus we talked about what statements could be interesting to look at. When we agreed on specifying our program a bit more, we went on google to find some inspiration for text. We agreed on making a program inspired by Geoff Cox, which we got presented to by the Symposium. 
Our collaboration worked out very fine, as we supplemented each other very good, and ended up making a program we are proud of! 


**What's the program about?**

Our program is an example of electronic literature, where we question what we see on the screen with the text: "WHAT IS THIS". With this question we want people to think about; what we see, is that really what it is? We chose to make the statement swaying, as that fitted well together with the fact that our program is a very abstract. 

We got our inspiration from Geoff Cox, from the exhibition Symposium back in February. He talked about a book called "ways of seeing", where he showed us a photo of a pipe, and asked what is was. Well, we said a pipe. But no - it is not a pipe, it is a photo of a pipe. Thus what is it, that we are showing on the screen? Is it text? Is it a sentence? Is it some kind of art? Or maybe a photo of something? Trying to answer our own questions, we decided to make a random list of words in a JSON file, of what it could be. These words pop up random in the background of the statement and disappear again shortly after. 
The colors don't have anything to do with our statement, thus in principle we could have made it black and white. But we chose colors to bright it up a bit. We have chosen a nice mint/ blue/ green color as our background and a pink color on the tekst and on the "artwork"/ lines on our statement. We choose white strokes around the text to bright it up a bit. 


**What is the aesthetic aspect of your program in particular to the relationship between code and language?**

Our program isn't a new kind of poetry or anything else fancy - but we made a statement, which is a question, whereas we try to answer it our self in the program: What do you see verses what is it? You can interpret our program as you like: The statement WHAT IS THIS could be considered as art or just a sentence - or maybe as something else? We have no wrong answers in our program - as the program is situated to each individual person. What do you think it is? 

If we look at the relationship between code and language in our program, both the computer and the person can read it. The question is, HOW the reader wants to interpret it. The computer just reads our code and executes it. This is related to the Speech Act, where the speech we deliver is acting. Our big question performs an action in which several possible answers appear in the background. Thus the sentences are doing much more than just being a sentence, they are acting/ performing in the act. 

The relationship between reader and the computer are totally separate, which Edsger Dijkstra expresses on page 26 in the book Speaking Code, Vocable Code from 2013: "whereas machines must be able to execute programs (without understanding them), people must be able to understand them (without executing them). These two activities are so utterly disconnected — the one can take place without the other". If we should seperate our program from our code, the code isn't readable for people with non-coding experience, but even though programming language isn't spoken as such, it express's particular qualities that come close to speech and even extend our understanding of speech (page 37). 

If we seperate the code and the executive program, we have to sets of programming. The code has both a legible state and an executable state, whereas both can be readable. Our legible state is readable as well, and in fact it could be looked as a part of the statement, as we choose specific words for each variable and for each function. Thus instead of just calling our JSON file: words, we called it "nothingness" and referred to a variable called "suggestions". We also used the word "continuity" for the variable which makes the program run in loop, etc.  Most of our words are specifically chosen to fit together with our executed program. Therefore our code itself is interesting to read. 

Codes can be quite interesting to read! It doesn't always have to be a well-running program, as the code itself can be a statement. An example of this is Mez Breeze, who invented a programming language called Mezangelle. This programming language can be interpreted in several ways - but most people interpret is as poetry. The interesting part of her language is the fact that it can't be executed. This really highlights how important the code is as well!  



**Freya Wattez, Æstetisk Software Programmering, 2. semester**

**Screenshot of atom-live-server:**
[ScreenShot](Skærmbillede.png)


**Link to my atom-live-server**
[Link](https://gl.githack.com/Annebeltz/ap2019/raw/master/Mini_ex8/index.html)


**References:**

Speaking code: vocable code, Cox, Geoff and McLean, Alex. Speaking Code. MIT Press, 2013.

Mezangelle, an Online Language for Codework and Poetry, Written by Aria Dean, dec 15, 2016. 

The symposium: Geoff Cox
