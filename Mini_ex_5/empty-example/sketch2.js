
var button;
var button2;
var button3;
var pressed = false;

function setup() {
 createCanvas(windowWidth, windowHeight);
 frameRate (10);  //Speed // How fast the throbber turns.

 textFont("BOLDITALIC",80);


 //1 knap press me
button = createButton('Press Me');
button.position(690, 300);
button.mousePressed(mainland);

//2 Knap Take action
button2 = createButton('Take Action');
button2.position(685, 400);
button2.mousePressed(action);

//3 knap - future
button = createButton('The Earths future');
button.position(675, 500);
button.mousePressed(remove);
}

function draw() {

  fill(200,100);  //the number of colourstring
  rect(0,0,width,height);
  Throbber(20); //The amount of circles in the throbber + the appearing of the throbber

//The globe
  noStroke();
  fill(20,75,200); //the colour of the globe
  circle(720,415,200);

  fill('hsb(160, 100%, 50%)');//the colour of "mainland" on the globe
  ellipse(630,415,70,150);
  ellipse(630,490,40,75);
  ellipse(640,530,20,50);
  ellipse(650,430,100,80);
  ellipse(595,350,20,100);
  ellipse(630,290,100,50);
  ellipse(710,260,200,75);
  ellipse(830,287,40,50);
  ellipse(660,350,30,10);
  ellipse(650,340,20,10);
  ellipse(840,420,160,100);
  ellipse(873,360,60,120);
  ellipse(860,440,100,200);
  ellipse(840,510,100,50);
  ellipse(830,540,20,10);

}


//Aktion knap 2
function action(){
pressed = true;
  text('The earth is not going to save itself',180,180);
  text('Everything is temporary',340,700);
  }

  //future knap 3
  function remove(){
    remove();
  }


function mainland(){
    //the "mainland"
    fill(20, 75, 200);//the colour of "mainland" on the globe
    ellipse(630,415,70,150);
    ellipse(630,490,40,75);
    ellipse(640,530,20,50);
    ellipse(650,430,100,80);
    ellipse(595,350,20,100);
    ellipse(630,290,100,50);
    ellipse(710,260,200,75);
    ellipse(830,287,40,50);
    ellipse(660,350,30,10);
    ellipse(650,340,20,10);
    ellipse(840,420,160,100);
    ellipse(873,360,60,120);
    ellipse(860,440,100,200);
    ellipse(840,510,100,50);
    ellipse(830,540,20,10);
  }


function Throbber(num) {
  push();
  translate(width/2, height/2); //move things to the center
  let cir = 360/num*(frameCount%num);  //which one among 9 possible positions.
  rotate(radians(cir));
noStroke();
  fill(20, 75, 200); //colour of the throbber <-- blue
  ellipse(205,205,185,185);  //the moving dots
  pop();

}
