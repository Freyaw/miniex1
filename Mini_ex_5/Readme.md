**What is the concept of your work? What do you want to express? What have you changed and why?**

I choose to re-make my throbber, as I didn’t have the skills to make it as wanted the last time. It turned out, that I still have to fight to get it working. My idea behind the throbber was that it should symbolize how time is changing our earth. As the throbber then turned around, the earth-globe should be more and more flood by water. This didn’t succeed the last time, but I did succeed in making a throbber spinning around a circle looking like the earth. But this time I wanted it to symbolize the global warming even more – I wanted it to express something like: “The earth can’t safe itself”. After my feed from Esther I tried to figure out, how I could succeed, but the function she suggested weren’t on p5Js, thus I had to find another way. So I started thinking outside the box – finding another alternative.
My goal was to make a throbber which symbolized the world screaming for help, as it can’t help itself. Thus I made three buttons, as we learned the other week, that people feel tempted to press buttons, thus if they pressed my three buttons they should get the message. The first button was the most difficult one, as I wanted to make an if-statement, as if you press the button named “press me”, the “mainland” should be removed, as the earth gets more and more flooded as times passes. I tried to make this function by using clear (didn’t work), remove (didn’t work) and hide (didn’t work), but in the end I just copied another “mainland”, where I changed the color to the same color as the water. Thus when you press the first button, the “mainland” won’t really disappear, but it looks like it has. The last thing I couldn’t figure out, was to make it static, as it only disappears fast – only the moment when you click the button.The second button says “take action”, as the result from pressing that button is text appearing, with the words: “The earth can’t safe itself” and “Everything is temporary”. Here the text appears really fast, which I thought was a nice touch, as the text highlights temporary, and the text is only showing fast. The last button is called “The earths future”, and when you press the button, the screen gets blank, as I make a remove-function. This should symbolize, that if we don’t do anything soon, the earth will soon disappear. 


**What does it mean by programming as a practice? What is the relation between programming and digital culture?**
Programming-language should be taught to a much broader perspective, and by that meaning that more people should learn it, as I has come an important aspect of our environment today. More people should learn to program because you get to think I different universes, you get a better understanding of the culture and the media and of course it would help with understanding and improve the society. That is exactly what Nick Montford is saying, in the text “Appendix A: Why program”, which is also highlighted in the following quote: “Understanding computation and having basic skills in programming allows researchers to question, refine, overturn, or further develop existing data representations, computational methods and theories”, page 262. And Douglas Rushkoof writes on page 273 in the text: “For the person who understands code, the whole world reveals itself as a series of decisions made by planners and designers for how the rest of us live”, 2010, page 140. 
Now we know how much computation there is in our environment and how important it is, thus why aren’t people learning code? My first assumption must be, that it just seems to difficult thus unmanageable. Engelbart, from the same text as mentioned above, had his attention on the challenge, and therefore he tried to form programming, thus it got easier to learn. 
Another theoretician highlighting the importance of programming is Annette Vee, whom wrote the text Coding Literacy, coding for everyone and the legacy of mass literacy. She uses the word “Computer Literacy” throughout the whole text, as it is a concept which underlines the importance, the flexibility and the power of writing with computers – writing in code. She also discusses why people code, whereas most people code because individual empowerment, as the computer takes up more in our everyday lives, the more control and creativity gives coding. She also talks about how many programmers are interested in how programming can revolutionize your thinking, which I also think is a very interesting aspect. 

**References:**
Vee, Annette, Coding Literacy, coding for everyone and the legacy of mass literacy
Montford, Nick, Appendix A: Why program


**Freya Wattez, Æstetisk Software Programmering, 2. semester**



Screenshot of atom-live-server:

[ScreenShot](Skærmbillede 4.png)


Screenshot of my coding-proces:

[ScreenShot](Skærmbillede 1.png)

[ScreenShot](Skærmbillede 2.png)

[ScreenShot](Skærmbillede 3.png)

Link to my atom-live-server

[Link](https://gl.githack.com/Freyaw/miniex1/raw/master/Mini_ex_5/empty-example/index.html)

