//variabler
let capture;
var button;
var button2;
var inverted = false;

function setup() {
  var c = createCanvas(windowWidth, windowHeight);
  c.position(0,0);
  
//webcam
capture=createCapture();
capture.hide();

background('#fae');

//tekststørrelse
textFont("BOLDITALIC",35);


//knap scan
button = createButton('Scan me');
button.position(660, 550);
button.mousePressed(scan);

//Knap resultater
button2 = createButton('Show results');
button2.position(650, 570);
button2.mousePressed(result);
}

function draw() {
//webcam
  if(capture.loadedmetadata){
    face=capture.get(200,30,300,300);
    image(face,550,250);

//Filter på scanknappen
    if(inverted) {
      filter('INVERT');
    }
  }
}

function scan(){
  //Scanningsfilteret
  inverted = true;
}

function result(){
  //Restultatet med tekstrubrikkerne
  //tekstrubrikkerne
  inverted = false;
  fill('rgba(0,255,0, 0.25)');
  text('HUMAN CAPTURED',535,620);
  text('SEX CAPTURED',900,100);
  text('AGE CAPTURED',150,100);
  text('HAIRCOLOR CAPTURED',200,750);
  text('BLUSHED CHEEKS CAPTURED',700,720);
  text('LOCATION CAPTURED',580,200);
  text('HAPPY 58%',300,300);
  text('DEPRESSED 8%',120,500);
  text('STRESSED 89%',900,500);
  text('BEAUTY 100%',1100,300);
  text('EXCITED 0%',1150,600);
}
