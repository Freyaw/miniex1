**Describe your sketch and explain what have been captured both conceptually and technically. How's the process of capturing?**


Before starting with this weekly coding-process, I searched on Transmediale 2015, as the festivalsubject was CAPTURE ALL. When entering their website, I noticed one quote, which I read several times whilst I was coding my project, as I used it as the fundamental thought behind my work: “In the logic of ‘capture all’, life is increasingly governed by never-ending predictive control”.  

My idea for this assignment was to make a livestream which should represent that people aren’t safe behind their computer, as the computer is watching all the time. As we had to capture something, I choose to make a button with “scan me” and another button right beneath saying “show result”. I don’t have the ability to make an actually capture, thus my program is just faking captures. 
Thus my program consists of a live webcam, a pink background and two buttons. When you press the first button saying “scan me”, the screen will change to filter mode, whereas the webcam will convert in colours and the background will switch between pink and green (as green is the opposite of pink). This effect should represent that the computer is scanning you, even though it is not really happening. Now you press the second button “show results”. When you click this button text will appear, which say statements as “Age captured”, “hair color captured”, “happy 58%”, etc. Thus the scan scanned the person in front of the screen, and captured the persons information and even the persons feelings (depressed, happy, stressed, etc.). 


To make this idea I had to use some specific functions in the codingproces, which I found on the p5js library. First of all I had to have a live webcam, whereas I didn’t chose capture.show(), as the filter won’t work on a HTML file. Thus I chose the webcam with the photos by using face.capture and loadedmetadata (it shows 60? photos each minute or something like that) instead of live webcam. To make the buttons I had to make two variables, thus I could create the buttons, and adding mousepressed. Here I used the if-statement for the first button, thus if the button “scan me” got pressed, the filter would show. The filter I found from the p5js library, thus it was easy to insert that as one of the functions. The text was made by adding text different places on the screen, and it would show when you pressed the result button. 


My coding process could have been made more complex, but I choose to keep it simple, as I understood every single step on the way – which is important for me. 


**Together with the assigned reading and coding process, how might this ex help you to think about or understand the data capturing process in digital culture?**


The assigned reading highlighted how we contribute to the data capture when we are online, and how buttons are a big impact on this subject. This wasn’t much new for me, as I work with Social Media with my job on a daily basis, where we use cookies, and sometimes send newsletters/ send adds only to the ones who visited our webshop in the last week. But it still makes you think: everything you do online is being captured – they can even see which pattern you use your mouse. I think it is scary to think about, as the “big brother is watching you” statement gets more and more real. In the article “The like economy: Social buttons and the data-intensive web”, written by Carolin Gerlitz and Anne Helmond from 2013, they emphasizes this on page 1360: “Being social online means being traced and contributing to value creation for multiple actors including Facebook and external webmasters". This is both interesting and frightening. 


They use our preferences and habits online to create adds and posts (and surely a lot of other things) that would fit into our routine. An example is cookies – if I like a dress and I visit it once, the next many many days the dress will appear as an ad everywhere on the social internet: Instagram, facebook, blogs, etc. Even worse: My boyfriend sent me his birthday wishes, and I opened the word document, and even though I didn’t open the links he sent with it, his birthday wishes were all over my social media. 


Thus with my project I tried to highlight how the computer can capture information of people, without even asking questions. I know that the webcam can’t do so at the moment, but they can see your preferences by just looking at your mouse movement, and there they aren’t asking any question – just looking at your movement. I hope my project contributed to seeing data capture as I wished it would.  



**References:**

https://transmediale.de/content/festival-theme

The like economy: Social buttons and the data-intensive web, an Artikle written by Carolin Gerlitz and Anne Helmond, from 2013




Freya Wattez, Æstetisk Software Programmering, 2. semester



**My product:**

Screenshot of atom-live-server in three stages:

[ScreenShot 1](Live1.png)

[ScreenShot 2](Live2.png)

[ScreenShot 3](Live 3.png)



Screenshot of my coding-proces:

[ScreenShot](Kode1.png)

[ScreenShot](Kode2.png)


Link to my atom-live-server - be attentive: it works on all internetservers except on Safari! 

[Link](https://gl.githack.com/Freyaw/miniex1/raw/master/Mini_ex_4/empty-example/index.html)
