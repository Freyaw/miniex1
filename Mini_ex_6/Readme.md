I had to go to Holland due to sickness in the family, thus my priorities have been elsewhere this week – I hope that’s ok  

**Describe how you program the objects and their related attributes and methods in your game.**

I first had high ambition of making my own version of Flappy Bird, and I started on that project in the beginning of the week, but then I got distracted as I had to go to Holland. Thus even though I had begun on a project, it felt like I couldn’t manage it. So I choose to make an easy project, where I made a class with a constructor and a method. 

I made a class called Shape, where the constructor only defines where it’s located. Then I made a moving function, where I choose to make it a random movement from the location I had placed the class. To make it a bit more interesting I choose to make the class consist of three objects: an ellipse, a triangle and a square. I filled those three objects with three different colors. 

Thus I haven’t made some kind of fancy game, but I have linked my first experiment on flappy bird underneath. I’ve shown that I understand the basics of class, constructor and method, even though my project doesn’t do something crazy and exciting.  

I could use some help though : my program is split in two? You can see that my program is split in two, where the first one is static and the other one moves. I have no idea why it does this. This was also one of my problems with my flappy-bird code.. 

**Based on Shiftman's videos, Fuller and Goffey's text and in-class lecture, what are the characteristics of object-oriented programming?**

After reading and watching our acquired readings/ videoes, I’ve learnt quite a lot about object-oriented programming. I was very confused after reading the text, but after the lecture I feel like I have a good understanding. 

Object-oriented programming is a course of action for computing, which shows programs in form of interactions between defined computational object. Thus it is a programming method, which for many – including me – makes programming look more manageable, as it splits the coding into different classes. 

In short a class is some kind of template, where you can organize your variables. A class specifiesthe structure of the object/ objects attributes and the actions. A class consists of a constructor, an movement/ method and some objects or an object. Objects in object orientated programming are groupings of data with methods which can be executed – it is something specific. 

The development of object-oriented programming has given a whole new aspect of interaction between user and computer. And this IS important, as we live in a digital world, where coding is a bigger part of our daily life then we first may think. For me – and maybe for you as well – the object-oriented programming has made it easier to read other peoples code, as it is structured so well. Maybe that will make it easier and more interesting for other people to read codes as well. 


Freya Wattez, Æstetisk Software Programmering, 2. semester

Screenshot of atom-live-server:
[ScreenShot](Skærmbillede.png)

Screenshot of my coding-proces:
[ScreenShot](Skærmbillede1.png)


Link to my atom-live-server
[Link](https://gl.githack.com/Freyaw/miniex1/raw/master/Mini_ex_6/empty-example/index.html)

Link to my flappy bird experiment won't work ! But it can be found in the map "forsøg". 
[Link]()
