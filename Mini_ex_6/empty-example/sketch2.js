let shape1;
let shape2;

function setup() {
createCanvas(600,800);
shape1 = new Shape();
shape2 = new Shape();
}

function draw(){
background(0,100,100);
shape1.move();
shape1.show();
shape2.move();
shape2.show();
}
class Shape { //class
  constructor(){   //constructor
    this.x=200;
    this.y=250;
  }
move(){ //method
  this.x=this.x+random(-8,8);
  this.y=this.y+random(-8,8);
}
show(){
  stroke(200);
  strokeWeight(6);
  //de tre objekter som bevæger sig random
  fill('#fae');
  ellipse(this.x,this.y,310,310);
  fill(255, 204, 100);
  triangle(this.x, this.y,30, 75, 58, 20, 86, 75);
  fill('rgba(0,255,0, 0.25)');
  square(this.x, this.y, 300, 20, 55);
}

}
