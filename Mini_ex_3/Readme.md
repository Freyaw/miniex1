 
**Describe  your throbber design, both conceptually and technically:**

When I first read the thesis statement, I immediately thought about coding something with symbolic meaning. Because I think a lot about global warming, this was my obvious choice. I was thinking about a throbber, which should be connected to earth globe. As the throbber runs around, the globe should become more and more flooded by water, as it should represent how we treat planet earth. But as I started the project, it turned out to be a lot harder then I first assumed.  

I started making a throbber with inspiration from Winnies lecture. I defined a function called function Throbber, whereas I called an ellipse, the rotation, the fill and translated it to the center and defined the size of the throbber. In this section I also made sure that it could turn around by using the function rotate. I used the function Push-Pop, so that I had more control. The Push-Pop function allowed me to experiment, and later return back to what I had before. At last I called the function Throbber under the function draw, and added 20 to define the amount of circles in the throbber I wanted. 

The speed of the throbber I defined in the function setup, and chose framerate 10. 

When the throbber succeeded, I made the globe in the middle of the throbber, and added many ellipses, which should look like the mainland. Here I changed the colors, thus it looked a bit more as planet earth. 

Hereafter I dreamt about adding a when-function, which should overflood the earth connected to the throbbers speed and laps. But I just couldn’t make it work. I tried several functions - including scale, if-statements, etc., but it didn’t work. I asked four co-students, and they also had to give up. 

But in conclusion I like the symbolic result, but it bothers me, that I can’t show my original idea. That’s a bummer! 


**Think about a throbber that you have encounted in digital culture – What do you think a throbber tells us, and/or hides, about?**

I associate a Throbber with a time thief. I feel like a Throbber steals my time, as I just sit there waiting for something to happen. At the same time I associate a throbber with an error – every time my computer shows a throbber I panic! But that depends on whether I’m watching something or I’m doing an important task on word, the internet, etc. Watching a video while a throbber appears feels natural, as you know, that the internet can be a bit slow sometimes, and that’s why it won’t load – it is in-progress. But if you are engaging with a project or something else – the computer can’t follow, thus a throbber appears and I panic. 

But when I think about it, a throbber is a good idea, as it’s better to see a loading-icon, than just an error or blank screen. The loading function gives you hope, as you feel like you get closer to an solution, whereas a blank screen just stands still. 

I’m not very technical, but I know that there is a lot of stuff (coding, errors, etc) behind the throbber. But the appearing of the throbber becomes an interface between the computional process and the visual communication. We have no idea what is happening, but it looks like that it’s processing, and we cross our fingers, and hope that it works soon again. 




Freya Wattez, Æstetisk Software Programmering, 2. semester



Screenshot of atom-live-server:

[ScreenShot](Skærmbillede.png)


Screenshot of my coding-proces:

[ScreenShot](Kodning1.png)

[ScreenShot](Kodning2.png)

Link to my atom-live-server

[Link](https://gl.githack.com/Freyaw/miniex1/raw/master/Mini_ex_3/empty-example/index.html)

