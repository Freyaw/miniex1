var bird;
var pipes =[];

function setup() {
createCanvas(400,600);
bird = new Bird();
pipes.push(new Pipe());

}

function draw(){
background(0); //black color
bird.update();
bird.show();

for (var i=0; i<pipes.lenght; i++){
  pipes[i].show();
  pipes[i].update();
}

}

function keyPressed(){
  if (key == ' '){
    bird.up();
    //console.log("SPACE");

  }
}

//"fuglen" i flappy bird som jeg har lavet en trekant.
function Bird(){
//position
  this.y = height/2;
  this.x = 64;

  this.gravity = 0.3;  //tyngdekraften som får fuglen til at falde.
  this.lift = -8; //løftet opad
  this.velocity = 0;//hastigheden den falder med

this.show = function(){
  fill(255); //white color
  ellipse(this.x, this.y, 32, 32);
}

this.up=function(){
  this.velocity += this.lift;

}

this.update = function() {
  this.velocity += this.gravity;
  this.velocity *= 0.9;
  this.y += this.velocity;

  if (this.y > height){
      this.y = height;
      this.velocity =0;

  }

  if (this.y < 0){
      this.y = 0;
      this.velocity =0;

  }

}

}

//funktionen til rørene
function Pipe() {
  this.top=random(height/2);
  this.bottom=random(height/2);
  this.x=width;
  this.w=20;
  this.speed=1;

  this.show=function(){
    fill(255);
    rect(this.x,0,this.w, this.top);
    rect(this.x,height-this.bottom,this.w, this.button);
  }

this.update=function(){

  this.x-=this.speed;
}

}
